﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Cinemachine;
using TMPro;

public class IntrusiveThoughtsScript : MonoBehaviour
{
    //HIERARCHY OBJECT REFERENCES
    public GameObject playerReference;
    public GameObject gameControllerReference;
    public GameObject ominousAudioHandlerReference;
    public GameObject pickupAudioHandlerReference;
    public GameObject ITAudioHandlerReference;
    public ItemCollectScript itemCollectScriptReference;
    public GameObject ScoreText;
    public Transform spawnPointReference;
    public Image donutUISprite;
    public Image gummyBearUISprite;
    public Image lollipopUISprite;
    public GameObject EscapeText;
    public GameObject gameOverText;
    public GameObject quitTextUIReference;

    //SHORTHAND VARIABLES
    public GameObject currentITtoFade;

    //DOTWEEN SHORTHANDS
    private float shrinkDuration; //how long it takes for the thought bubble DoScale() to complete its Tween
    private float shrinkPercentage; //what % the thought bubble shrinks each time DoScale() is called

    //VECTOR VALUES
    private Vector3 newThoughtBubbleSize;

    //ATTRIBUTE/STAT VARIABLES
    private int score;

    //CAMERA VARIABLES
    [SerializeField]
    private CinemachineVirtualCamera mainCam;
    //public Camera UICam;

    //AUDIO VARIABLES
    [SerializeField]
    private AudioClip[] ominousMusicArray; //array to hold the x number of ominous bg music variation
    private AudioClip[] itBubblePopArray;
    private AudioSource happyBgMusicAudioSource;
    private AudioSource ominousBgMusicAudioSource; //audio Source for the BG Music
    private AudioClip ominousMusicClip;

    private AudioSource pickupAudioSource;
    private AudioClip pickupAudioClip;

    private AudioSource itAudioSource;
    private AudioClip itAudioClip;

    //INTRUSIVE THOUGHT OBJECT REFERENCES
    public GameObject[] IntrusiveThoughtArray;
    public GameObject intrusiveThought0;
    public GameObject intrusiveThought1;
    public GameObject intrusiveThought2;
    public GameObject intrusiveThought3;
    public GameObject intrusiveThought4;
    public GameObject intrusiveThought5;
    public GameObject intrusiveThought6;
    public GameObject intrusiveThought7;
    public GameObject intrusiveThought8;
    public GameObject intrusiveThought9;
    public GameObject intrusiveThought10;
    public GameObject intrusiveThought11;
    public GameObject intrusiveThought12;
    public GameObject intrusiveThought13;
    public GameObject intrusiveThought14;
    public GameObject intrusiveThought15;

    public int ITManualArrayIndex;
    public int ITManualArrayFadeOutIndex;

    //TIMER VARIABLES
    private float camShakeDuration;
    private float currentTimeCameraShake;

    private float ITDuration;
    private float currentTimeIT;

    //CONDITIONAL VARIABLES
    private bool ITOnScreen;
    private bool hasDonut;
    private bool hasGummyBear;
    private bool hasLollipop;
    public bool playerIsDead;
    public bool playerInitiatedEndGame;
    public bool playerIsInEscapeRoom;
    public bool quitUIvisible;

    void Start()
    {
        //Debug.Log(!playerReference.GetComponent<Animator>().GetBool("dead"));

        camShakeDuration = 0.5f;
        ITDuration = 5f;

        shrinkDuration = 0.3f;
        shrinkPercentage = 0.15f;
        

        //populate the array with specific Text objects
        IntrusiveThoughtArray = new GameObject[] {
            intrusiveThought0, intrusiveThought1, intrusiveThought2, intrusiveThought3,
            intrusiveThought4, intrusiveThought5, intrusiveThought6, intrusiveThought7,
            intrusiveThought8, intrusiveThought9, intrusiveThought10, intrusiveThought11,
            intrusiveThought12, intrusiveThought13, intrusiveThought14, intrusiveThought15};

        currentITtoFade = IntrusiveThoughtArray[ITManualArrayFadeOutIndex]; //assign shorthand

        //populate the array with specific audio clips
        ominousBgMusicAudioSource = ominousAudioHandlerReference.GetComponent<AudioSource>();
        ominousMusicArray = new AudioClip[]
        {
            (AudioClip)Resources.Load("OminousBGMusicVariation0"),
            (AudioClip)Resources.Load("OminousBGMusicVariation1"),
            (AudioClip)Resources.Load("OminousBGMusicVariation2"),
            (AudioClip)Resources.Load("OminousBGMusicVariation3")
        };

        //populate the array with specific audio clips
        itAudioSource = ITAudioHandlerReference.GetComponent<AudioSource>();
        itBubblePopArray = new AudioClip[]
        {
            (AudioClip)Resources.Load("BubblePop0"),
            (AudioClip)Resources.Load("BubblePop1"),
            (AudioClip)Resources.Load("BubblePop2"),
            (AudioClip)Resources.Load("BubbleShrink")
        };

        //assign audioSource 
        happyBgMusicAudioSource = gameControllerReference.GetComponent<AudioSource>();
        happyBgMusicAudioSource.clip = (AudioClip)Resources.Load("HappyBgMusic");

        //assign audioSource 
        pickupAudioSource = pickupAudioHandlerReference.GetComponent<AudioSource>();
        pickupAudioSource.clip = (AudioClip)Resources.Load("Coin");


    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape) && quitUIvisible == false)
        {
            print("player pressed Escape in-game");
            //show confirmation UI: "Quit Game? Y/N
            //quitTextUIReference.SetActive(true);
            quitUIvisible = true;
            quitTextUIReference.transform.DOMove(new Vector3(960f, 640f, 0f), 0.3f);
        }

        //if confirmation UI is visible/active + player presses Y, quit application
        if (quitUIvisible && Input.GetKeyDown(KeyCode.Y))
        {
            print("player pressed Y to quit application");
            Application.Quit();
        }

        //press N or Escape key to resume game
        else if ((quitUIvisible && Input.GetKey(KeyCode.N)) /*||
            (quitUIvisible && Input.GetKeyDown(KeyCode.Escape))*/)
        {
            //quitTextUIReference.SetActive(false); not working bc it's a deactivated object

            quitUIvisible = false;
            quitTextUIReference.transform.DOMove(new Vector3(960f, 1580f, 0f), 0.3f);
        }
        //print("ITManual Array FadeOut Index is " + ITManualArrayFadeOutIndex);
        //print ("xPos of current Intrusive Thought to Fade is " + currentITtoFade.transform.position.x);
        /*if (ITOnScreen)
        {
            ITTimer();
        }*/

        /*if (mainCam.GetCinemachineComponent
            <Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain > 0)
        //if camera is currently shaking
        {
            //start timer to reset camera and stop shaking
            CameraShakeTimer();
        }*/

        if (ITManualArrayIndex ==IntrusiveThoughtArray.Length || playerIsInEscapeRoom) //if final IT has been triggered
        {
            //display UI prompting player to press ESC key
            ITTimer(); //repurpose ITTimer to start a little delay before UI appears //triggers UI to appear once completed
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            print("player pressed E key");
            
            if (EscapeText.activeInHierarchy && !playerInitiatedEndGame)
            {
                //reset cam shake to zero
                mainCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
                mainCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 0;
                EscapeText.SetActive(false);
                playerInitiatedEndGame = true;
                print("escape text set to disabled");
                //currentTimeIT = 0f; idk if i need this if i destroy the trigger "EscapeRoom"

                PlayHappyMusic();
                DisperseIntrusiveThoughts();
            }
        }
            
    }

    //UI FUNCTIONS

    public void UpdateScoreText(int scoreIncrease)
    {
        score += scoreIncrease;
        ScoreText.GetComponent<TextMeshProUGUI>().SetText("" + score.ToString("000000"));
    }

    void UpdateDonutUI()
    {
        if (hasDonut)
        {
            donutUISprite.color = new Color32(255, 255, 255, 255); //full color full opacity
        }
    }

    void UpdateGummyBearUI()
    {
        if (hasGummyBear)
        {
            gummyBearUISprite.color = new Color32(255, 255, 255, 255); //full color full opacity
        }
    }

    void UpdateLollipopUI()
    {
        if (hasLollipop)
        {
            lollipopUISprite.color = new Color32(255, 255, 255, 255); //full color full opacity
        }
    }

    void ShowEscapeUI()
    {
        if (EscapeText.activeInHierarchy == false && !playerInitiatedEndGame)
        {
            print("ShowEscapeUI function called");
            PlayOminousMusic();
            EscapeText.SetActive(true);
            EscapeText.gameObject.transform.DOScale(4.5f, 15f);//.SetLoops(2, LoopType.Yoyo);
            //UICam.DOShakePosition()
        }
        
    }

    //INTRUSIVE THOUGHT FUNCTIONS

    void TriggerIntrusiveThought(int ITArrayIndex)
    {
        //PlayOminousMusic();
        if (ITManualArrayIndex == 0)
        {
            Debug.Log("TriggerIntrusiveThought() function called");
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(1743f, 943f, 0f), 0.3f); //tween text to center of screen
            //IntrusiveThoughtArray[ITManualArrayIndex].transform.DOScale(new Vector3(1.3f, 1.3f), 8f); //scale it up
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

            //set ITDuration timer values
            //currentTimeIT = ITDuration;
        }

        else if (ITManualArrayIndex==1)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(275f, 870f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }

        else if (ITManualArrayIndex == 2)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(1158f, 159f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 3)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(747f, 741f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 4)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(542f, 144f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 5)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(415, 450f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 6)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(1438f, 548f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 7)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(1661f, 152f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 8)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(960f, 540f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 9)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(1496f, 758f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 10)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(546f, 423f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 11)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(1275f, 436f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 12)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(650f, 660f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 13)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(1294f, 361f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 14)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(1427f, 834f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

        }
        else if (ITManualArrayIndex == 15)
        {
            IntrusiveThoughtArray[ITManualArrayIndex].transform.DOMove(new Vector3(924f, 515f, 0f), 0.3f); //tween text to center of screen
            ITManualArrayIndex++;
            ITOnScreen = true; //so player doesn't trigger IT twice
            Debug.Log("ITManualArrayIndex is now " + ITManualArrayIndex);

            currentTimeIT = ITDuration; //assign values to timer variables;

        }


    }

    void ShrinkIntrusiveThought()
    {
        currentITtoFade.transform.DOScale(new Vector3(
                    currentITtoFade.transform.localScale.x - currentITtoFade.transform.localScale.x * shrinkPercentage,
                    currentITtoFade.transform.localScale.y - currentITtoFade.transform.localScale.y * shrinkPercentage, 1f),
                    shrinkDuration);
    }


    void EraseIntrusiveThought() //for final version, trigger this when IntrusiveThoughtTimer is complete
    {
        currentTimeCameraShake = camShakeDuration;
        IntrusiveThoughtArray[ITManualArrayIndex-1].transform.DOMove(new Vector3(1500f, Random.Range(-100, 400), 0f), 0.3f); //tween text
        mainCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 3f;
        mainCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 6.5f;
    }

    void DisperseIntrusiveThoughts()
    {
        //UICam.DOShakePosition(2, 0.3f, 40, 90, true);
        IntrusiveThoughtArray[0].transform.DOMove(new Vector3(-960f, 2000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[1].transform.DOMove(new Vector3(2880f, 2000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[2].transform.DOMove(new Vector3(2880f, -1000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[3].transform.DOMove(new Vector3(-960f, -1000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[4].transform.DOMove(new Vector3(-960f, 2000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[5].transform.DOMove(new Vector3(2880f, 2000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[6].transform.DOMove(new Vector3(-960f, -1000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[7].transform.DOMove(new Vector3(2880f, -1000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[8].transform.DOMove(new Vector3(-960f, 2000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[9].transform.DOMove(new Vector3(2880f, 2000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[10].transform.DOMove(new Vector3(2880f, -1000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[11].transform.DOMove(new Vector3(-960f,-1000f,0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[12].transform.DOMove(new Vector3(-960f, 2000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[13].transform.DOMove(new Vector3(2880f, 2000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[14].transform.DOMove(new Vector3(-960f, -1000f, 0f), 0.3f); //tween text to center of screen
        IntrusiveThoughtArray[15].transform.DOMove(new Vector3(2880f, -1000f, 0f), 0.3f); //tween text to center of screen
    }

    /*//ANIMATION CHECK FUNCTIONS
    bool AnimatorIsPlaying(string stateName)
    {
        return AnimatorIsPlaying(stateName) && playerReference.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Player-Jump");
    }*/

    //TRIGGER FUNCTIONS

    private void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.CompareTag("ITTrigger") && ITManualArrayIndex <16 && !playerInitiatedEndGame/*&& !ITOnScreen*/)
        {
            Debug.Log("player collided with collider tagged 'ITTrigger'");
            PlayBubblePop();
            TriggerIntrusiveThought(ITManualArrayIndex);
            Destroy(other); //so player doesn't trigger it twice by accident
            //print("xPos of Intrusive Thought is " + currentITtoFade.transform.position.x);
        }

        else if (other.CompareTag("EscapeRoom"))
        {
            currentTimeIT = ITDuration; //assign values to timer variables;
            playerIsInEscapeRoom = true;
            Destroy(other);
        }

        else if (/*playerReference.GetComponent<Animator>().GetBool("dead") && */other.CompareTag("Enemy")) //&& player is not dead)
        {
            //Debug.Log(playerReference.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Player-Jump")); //returns T/F
            /*if (/*!playerIsDead
            playerReference.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Player-Jump"))
            {
                //bruh
            }*/

            //MAYBE YOU JUST GET SCORE FROM ENEMIES, NO FADEOUT() TRIGGER TO AVOID AUDIO BUGS
            UpdateScoreText(100);
            /*if (ITOnScreen && currentITtoFade.transform.position.x <= 1920
                && currentITtoFade.transform.position.x >= 0)
            {
                PlayBubbleShrink();
                currentITtoFade.GetComponent<FadeOutScript>().FadeOut();
                Debug.Log("FadeOut() triggered");
            }*/
            Debug.Log("player collided with enemy");
            //MAYBE YOU JUST GET SCORE FROM ENEMIES, NO FADEOUT() TRIGGER TO AVOID AUDIO BUGS

        }

        else if (other.CompareTag("Pickup"))
        {
            pickupAudioSource.Play();
            UpdateScoreText(50);
            /*if (ITOnScreen)
            {
                currentITtoFade.GetComponent<FadeOutScript>().FadeOut();
                Debug.Log("FadeOut() triggered");
            }*/
            //Destroy(other.gameObject);
        }

        else if (other.CompareTag("Donut"))
        {
            itemCollectScriptReference = GameObject.FindWithTag("DonutCollect").GetComponent<ItemCollectScript>(); //get script reference
            itemCollectScriptReference.playerEnteredTrigger = true;

            pickupAudioSource.Play();
            UpdateScoreText(100);
            hasDonut = true;
            UpdateDonutUI();

            if (ITOnScreen && currentITtoFade.transform.position.x <= 1920
                && currentITtoFade.transform.position.x >= 0)
            {
                PlayBubbleShrink();

                //TRIGGER ITEM COLLECT SCRIPT
                itemCollectScriptReference.TweenItemToThoughtBubble();

                ShrinkIntrusiveThought();
                currentITtoFade.GetComponent<FadeOutScript>().FadeOut();
                
                Debug.Log("FadeOut() triggered");
            }
            Destroy(other.gameObject);
        }

        /*else if (other.CompareTag("PurityDonut"))
        {
            pickupAudioSource.Play();
            UpdateScoreText(1000);
            hasDonut = true;
            UpdateDonutUI();
            if (ITOnScreen && currentITtoFade.transform.position.x <= 1920
                && currentITtoFade.transform.position.x >= 0)
            {
                currentITtoFade.GetComponent<FadeOutScript>().FadeOut();
                Debug.Log("FadeOut() triggered");
            }
            Destroy(other.gameObject);
        }*/

        else if (other.CompareTag("GummyBear"))
        {
            itemCollectScriptReference = GameObject.FindWithTag("GummyBearCollect").GetComponent<ItemCollectScript>(); //get script reference
            itemCollectScriptReference.playerEnteredTrigger = true;

            pickupAudioSource.Play();
            UpdateScoreText(500);
            hasGummyBear = true;
            UpdateGummyBearUI();

            if (ITOnScreen && currentITtoFade.transform.position.x <= 1920
                && currentITtoFade.transform.position.x >= 0)
            {
                PlayBubbleShrink();
                
                //TRIGGER ITEM COLLECT SCRIPT
                itemCollectScriptReference.TweenItemToThoughtBubble();

                ShrinkIntrusiveThought();
                currentITtoFade.GetComponent<FadeOutScript>().FadeOut();
                
                Debug.Log("FadeOut() triggered");
            }
            Destroy(other.gameObject);
        }

        else if (other.CompareTag("Lollipop"))
        {
            itemCollectScriptReference = GameObject.FindWithTag("LollipopCollect").GetComponent<ItemCollectScript>(); //get script reference
            itemCollectScriptReference.playerEnteredTrigger = true;
            

            pickupAudioSource.Play();

            UpdateScoreText(250);
            
            hasLollipop = true;
            UpdateLollipopUI();

            if (ITOnScreen && currentITtoFade.transform.position.x <= 1920
                && currentITtoFade.transform.position.x >= 0)
            {
                PlayBubbleShrink();

                //TRIGGER ITEM COLLECT SCRIPT
                itemCollectScriptReference.TweenItemToThoughtBubble();
                //Debug.Log("XXXXXXXXXXXXXTweenItemToThoughtBubble() called for UI XXXXXXXXXXXXXX");

                ShrinkIntrusiveThought();
                currentITtoFade.GetComponent<FadeOutScript>().FadeOut();

                Debug.Log("FadeOut() triggered");
            }
            Destroy(other.gameObject);
            //print("Lollipop object destroyed");
        }

        else if (other.CompareTag("Checkpoint"))
        {
            spawnPointReference.position = new Vector2(other.transform.position.x, other.transform.position.y);
        }
        else if (other.CompareTag("GameOverMessage"))
        {
            if (!gameOverText.activeInHierarchy)
            {
                //happyBgMusicAudioSource.Pause(); //idk if i want to keep this
                gameOverText.SetActive(true);
            }
            
        }
    }

    //TIMER FUNCTIONS

    void ITTimer()
    {
        if (currentTimeIT >0f)
        {
            Debug.Log("countdown to EscapeUI prompt is " + currentTimeIT);
            currentTimeIT -= 1f * Time.deltaTime; //start counting down
            if (currentTimeIT <= 0 && !playerInitiatedEndGame)
            {
                currentTimeIT = 0f;
                //PlayHappyMusic();
                //EraseIntrusiveThought();
                //ITOnScreen = false;
                ShowEscapeUI();

                //start camera shake
                mainCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 3f;
                mainCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 6.5f;

                //return; //currentTimeIT = 100f; //so that it doesn't get stuck here
            }
        }
        

    }

    void CameraShakeTimer()
    {
        Debug.Log("currentTimeCameraShake is " + currentTimeCameraShake);
        currentTimeCameraShake -= 1f * Time.deltaTime; //start counting down
        if (currentTimeCameraShake <= 0)
        {
            //reset camera shake to none
            mainCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0;
            mainCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = 0;
            Debug.Log("amp and freq values reset to 0");
        }
    }

    //AUDIO FUNCTIONS

    void PlayBubblePop()
    {
        int index = Random.Range(0, itBubblePopArray.Length-2); //length -2 because the last index is for the shrinking SFX
        itAudioClip = itBubblePopArray[index];
        itAudioSource.clip = itAudioClip;
        itAudioSource.Play();
    }

    void PlayBubbleShrink()
    {
        itAudioSource.clip = itBubblePopArray[itBubblePopArray.Length-1]; //gets last position in array (which is reserved for the shrink SFX)
        itAudioSource.Play();
    }

    void PlayOminousMusic()
    {
        print("PlayOminousMusic() function called");
        happyBgMusicAudioSource.Pause();

        int index = Random.Range(0, ominousMusicArray.Length);
        ominousMusicClip = ominousMusicArray[index];
        ominousBgMusicAudioSource.clip = ominousMusicClip;
        ominousBgMusicAudioSource.Play();
    }

    void PlayHappyMusic()
    {
        ominousBgMusicAudioSource.Stop();

        happyBgMusicAudioSource.Play();
    }
}
