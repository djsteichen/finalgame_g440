﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class FadeOutScript : MonoBehaviour
{
    public IntrusiveThoughtsScript ITScriptReference;

    float startingAlpha;
    public float currentAlpha;
    public bool isTransparent;
    private int timesFaded = 1;

    // Start is called before the first frame update
    void Start()
    {
        startingAlpha = 255/255f; //because alpha values for DoFade are between 0 and 1
        currentAlpha = startingAlpha;
    }

    //CUSTOM FUNCTIONS
    public void FadeOut()
    {
        
        currentAlpha -= 85/255f; //fade out by 33.33% each time FadeOut() is called
        //Debug.Log("Current Alpha value is " + currentAlpha);

       /* if (timesFaded ==3)
        {
            isTransparent = true;
        }*/
        if (currentAlpha<= 0/255f &&
            ITScriptReference.ITManualArrayFadeOutIndex< ITScriptReference.IntrusiveThoughtArray.Length-1)
        {
            Debug.Log("Current Alpha value within if statement is " + currentAlpha);
            ITScriptReference.ITManualArrayFadeOutIndex++;

            //updates the shorthand variable with the current ITManualArrayFadeOutIndex;
            ITScriptReference.currentITtoFade = ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex];
            //if you move the line directly above to before the increase in ITManualArrayFadeOutIndex, 
            //the shrink() works how it should but the DoFade() doesn't
            print("ITManualArrayFadeOutIndex is " + ITScriptReference.ITManualArrayFadeOutIndex);
        }

        this.gameObject.GetComponentInChildren<SVGImage>().DOFade(currentAlpha, 1f); //fade bg UI image
        this.gameObject.GetComponentInChildren<TextMeshProUGUI>().DOFade(currentAlpha, 1f); //fade text
        //timesFaded++;
    }
}
