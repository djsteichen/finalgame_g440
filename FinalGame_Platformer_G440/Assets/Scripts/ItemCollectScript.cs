﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class ItemCollectScript : MonoBehaviour
{

    //reference to player object
    //[SerializeField]
    private IntrusiveThoughtsScript ITScriptReference;

    //[SerializeField]
    private GameObject playerReference;

    private Color transparencyColor;
    private Color opaqueColor;

    private int numberOfTimesTriggered =1;

    private float fadeDuration =0.3f;
    public bool isCollected;
    public bool playerEnteredTrigger;
    public bool transparencyTimerInitiated;

    //TIMER VARIABLES
    private float currentTime;
    private float countdownToTriggerTransparency; //should be the same value as however long the DoFade is for the image
    private bool timerCompleted;

    // Start is called before the first frame update
    void Start()
    {
        transparencyColor = new Color(255f, 255f, 255f, 0f);
        opaqueColor = new Color(255f, 255f, 255f, 1f);

        playerReference = GameObject.FindWithTag("Player");
        ITScriptReference = playerReference.GetComponent<IntrusiveThoughtsScript>();

        countdownToTriggerTransparency = fadeDuration;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isCollected && playerEnteredTrigger)
        {
            isCollected = true;
            //TweenItemToThoughtBubble();
            
            
            //this.transform.position = playerReference.transform.position;
        }

        if (transparencyTimerInitiated && currentTime>0f)
        {
            TriggerTransparencyTimer();
        }

        if (!isCollected && !playerEnteredTrigger && timerCompleted && this.gameObject.GetComponent<Image>().color.a == 0)
        {
            this.gameObject.transform.position = new Vector2(960f, 540f); //reset UI image position to middle of screen;
        }
    }

    public void TweenItemToThoughtBubble()
    {
        //set timer values and initiate timer
        currentTime = countdownToTriggerTransparency;
        transparencyTimerInitiated = true;

        print("XXXXXXXXTestTweenMethod() thinks FadeOutIndex is " + ITScriptReference.ITManualArrayFadeOutIndex + "XXXXXXXX");
        this.gameObject.GetComponent<Image>().color = opaqueColor; //make the UI image visible

        //move UI image to the current FadeOutIndex thought bubble's position
        this.transform.DOMove(ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].GetComponentInChildren<SVGImage>().transform.position, fadeDuration);

        
    }

    void OLDVERSIONTweenItemToThoughtBubble()
    {
        /*print("thought bubble current alpha: " + 
            ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].GetComponent<FadeOutScript>().currentAlpha);*/

        //print(170 / 255f);

        //Debug.Log(ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].transform.position.x);
        if (ITScriptReference.ITManualArrayFadeOutIndex >=0 && ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].transform.position.x <= 1920
                && ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].transform.position.x >= 0
                /*&& ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].GetComponent<FadeOutScript>().isTransparent == true*/)
        //if IT is on screen
        {
            print("XXXXXXXXXXXXX TweenItemToThoughtBubble() called; if statement triggered XXXXXXXXXXXXX");
            this.gameObject.GetComponent<Image>().color = opaqueColor;
            this.transform.DOMove(ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].GetComponentInChildren<SVGImage>().transform.position, fadeDuration);
            //if opacity is between 34%-100%

            //set timer values and initiate timer
            currentTime = countdownToTriggerTransparency;
            transparencyTimerInitiated = true;

            
        }

        /*else if (/*ITScriptReference.ITManualArrayFadeOutIndex >= 0 && ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].transform.position.x <= 1920
                && ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].transform.position.x >= 0
                && ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].GetComponent<FadeOutScript>().isTransparent == false)
        {
            this.gameObject.GetComponent<Image>().color = opaqueColor;
            this.transform.DOMove(ITScriptReference.IntrusiveThoughtArray[ITScriptReference.ITManualArrayFadeOutIndex].GetComponentInChildren<SVGImage>().transform.position, fadeDuration);
            //if opacity is at 33%

            //set timer values and initiate timer
            currentTime = countdownToTriggerTransparency;
            transparencyTimerInitiated = true;
        }*/
    }

    void MakeTransparent()
    {
        this.gameObject.GetComponent<Image>().DOFade(0f, 0.1f); //make UI image disappear, but keep gameObj's functionality
    }

    void TriggerTransparencyTimer()
    {
        Debug.Log("Time before Transparency initiated: " + currentTime);
        if (currentTime>0)
        {
            timerCompleted = false;
            currentTime -= 1f * Time.deltaTime;
            if(currentTime<= 0)
            {
                currentTime = 0f;

                MakeTransparent();
                Debug.Log("Item set to transparent");

                //reset variable values so that it works the next n number of times
                timerCompleted = true;
                playerEnteredTrigger = false;
                isCollected = false;
                transparencyTimerInitiated = false;
            }
            
        }
        
        
    }
}
