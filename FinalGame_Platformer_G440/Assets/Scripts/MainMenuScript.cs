﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    private float currentTime;
    private float countdownTime =5f;

    public GameObject startTextReference;
    

    // Start is called before the first frame update
    void Start()
    {
        currentTime = countdownTime; //assign timer values
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= 1f * Time.deltaTime; //start timer
        if (currentTime<=0)
        {
            if (startTextReference.activeInHierarchy==false)
            {
                startTextReference.SetActive(true);
            }
        }

        if (startTextReference.activeInHierarchy && Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene("Main");
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            print("player pressed Escape key");
            Application.Quit();
            //quit code
        }
    }
}
