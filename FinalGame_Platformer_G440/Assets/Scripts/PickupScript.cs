﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PickupScript : MonoBehaviour
{
    private AudioSource pickupAudioSource; //audio Source for the BG Music
    private AudioClip pickupAudioClip;

    private Vector3 startingPos;
    private float doMoveDuration;
    private float yPosChange;

    // Start is called before the first frame update
    void Start()
    {
        pickupAudioSource = this.GetComponent<AudioSource>();
        pickupAudioSource.clip = (AudioClip)Resources.Load("Coin");

        doMoveDuration = 0.75f;
        yPosChange = 0.15f;

        startingPos = this.gameObject.transform.position;
        //this.gameObject.transform.DOMoveY(startingPos.y + 0.15f, 0.75f); //initial movement
    }

    // Update is called once per frame
    void Update()
    {
        /*if (this.gameObject.transform.position.y == startingPos.y + 0.15f)
        {
            Debug.Log("why u no move");
            this.gameObject.transform.DOMoveY(startingPos.y - 0.15f, 0.75f); //Tween down
        }

        else if (this.gameObject.transform.position.y == startingPos.y - 0.15f)
        {
            this.gameObject.transform.DOMoveY(startingPos.y + 0.15f, 0.75f); //Tween up
        }*/
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        /*if(other.CompareTag("Player"))
        {
            pickupAudioSource.Play();
        }*/
        
    }
}
